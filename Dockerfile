# Stage 1
FROM node:10-alpine as build-step
RUN mkdir -p /app
WORKDIR /app
COPY package.json /app
RUN npm install
RUN npm install -g @angular/cli

COPY . /app
RUN npm run build --prod

EXPOSE 4200
CMD ng serve --host 0.0.0.0
