/**
 @author: Jakub Gzyl, 2021
 **/

import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {Car, CarOption} from "../../models";

@Component({
  selector: 'app-car-options-dialog',
  templateUrl: './car-options-dialog.component.html',
  styleUrls: ['./car-options-dialog.component.css']
})
export class CarOptionsDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public car: Car) {}

  ngOnInit(): void {
  }

}
