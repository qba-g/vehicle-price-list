/**
 @author: Jakub Gzyl, 2021
 **/

import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {Car} from "./models";
import {HttpClient} from "@angular/common/http";
import {environment} from "../environments/environment";
import {take} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class CarService {

  static BACKEND_SERVER = environment.backend_server;
  static GET_CARS_URL = '/car/list';
  static DELETE_CAR_URL = '/car/{id}';
  static POST_CAR_URL = '/car';
  static PUT_CAR_URL = '/car';

  constructor(private httpClient: HttpClient) {
  }

  getCars(): Observable<Car[]> {

    const url = CarService.BACKEND_SERVER + CarService.GET_CARS_URL;

    return this.httpClient.get<Car[]>(url, {responseType: 'json'}).pipe(take(1));
  }

  updateCar(car: Car): Observable<Car> {

    const url = CarService.BACKEND_SERVER + CarService.PUT_CAR_URL;
    return this.httpClient.put<Car>(url, car, {responseType: 'json'}).pipe(take(1));
  }

  addCar(car: Car): Observable<Car> {

    const url = CarService.BACKEND_SERVER + CarService.POST_CAR_URL;
    return this.httpClient.post<Car>(url, car, {responseType: 'json'}).pipe(take(1));
  }

  deleteCar(carId: number): Observable<any> {

    const url = CarService.BACKEND_SERVER + CarService.DELETE_CAR_URL.replace('{id}', '' + carId);

    return this.httpClient.delete(url).pipe(take(1));
  }
}
