/**
 @author: Jakub Gzyl, 2021
 **/

import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {ApplicationError} from "../errors";

@Component({
  selector: 'app-error-dialog',
  templateUrl: './error-dialog.component.html',
  styleUrls: ['./error-dialog.component.css']
})
export class ErrorDialogComponent implements OnInit {

  error: string = '';

  constructor(@Inject(MAT_DIALOG_DATA) public applicationError: ApplicationError) {
    this.error = applicationError.errorMessage;
  }

  ngOnInit(): void {
  }

}
