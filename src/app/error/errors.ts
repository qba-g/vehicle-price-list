/**
 @author: Jakub Gzyl, 2021
 **/

export class ApplicationError extends Error{
  constructor(public errorMessage: string, public originalError?: string, public errorCode?: string ){
    super(errorMessage);
    Object.setPrototypeOf(this, new.target.prototype);
  }
}
