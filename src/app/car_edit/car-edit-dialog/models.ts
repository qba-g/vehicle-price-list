/**
 @author: Jakub Gzyl, 2021
 **/

import {Car, CarOption, DialogMode} from "../../models";

export class CarDialogData {
  mode?: DialogMode;
  data?: Car;
  optionsData?: CarOption[];
}
