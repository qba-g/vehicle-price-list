/**
 @author: Jakub Gzyl, 2021
 **/

import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Car, CarOption, DialogMode} from "../../models";
import * as _ from "lodash";
import {CarDialogData} from "./models";


@Component({
  selector: 'app-car-edit-dialog',
  templateUrl: './car-edit-dialog.component.html',
  styleUrls: ['./car-edit-dialog.component.css']
})
export class CarEditDialogComponent implements OnInit {

  dialogMode: DialogMode = DialogMode.VIEW;

  dialogModel: Car = new Car(0, '', '', '');

  newOptionName: string = '';
  newOptionPrice?: number;

  constructor(
    public dialogRef: MatDialogRef<CarEditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public carDialogData: CarDialogData) {

    if (carDialogData !== null) {
      this.dialogMode = carDialogData.mode?carDialogData.mode: DialogMode.VIEW;
      this.dialogModel = carDialogData.data? _.cloneDeep(carDialogData.data): new Car(0, '', '', '');
      this.dialogModel.options = carDialogData.optionsData? _.cloneDeep(carDialogData.optionsData): [];
    }

  }

  ngOnInit(): void {
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  addOption() {
    const carNew = new CarOption(0, this.newOptionName, this.newOptionPrice);
    carNew.initUiId();

    this.dialogModel.options.push(carNew);
    this.clearOptionEditors();
  }

  clearOptionEditors() {
    this.newOptionName = '';
    this.newOptionPrice = undefined;
  }

  deleteOption(optionItem: CarOption) {

    if (optionItem) {
      _.remove<CarOption>(this.dialogModel.options, item => {
          return item && item.id === optionItem.id && item.uiId === optionItem.uiId
        }
      );
    }
  }
}
