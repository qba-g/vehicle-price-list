/**
 @author: Jakub Gzyl, 2021
 **/

import {Component, OnInit, ViewChild} from '@angular/core';
import {Car, DialogMode} from "./models";
import {CarOptionsDialogComponent} from "./car_options/car-options-dialog/car-options-dialog.component";
import {MatDialog} from "@angular/material/dialog";
import {CarEditDialogComponent} from "./car_edit/car-edit-dialog/car-edit-dialog.component";
import {CarDialogData} from "./car_edit/car-edit-dialog/models";
import {MatTable} from "@angular/material/table";
import {CarService} from "./car.service";
import {CarOptionsService} from "./car-options.service";
import * as _ from "lodash";
import {mergeMap} from "rxjs/operators";
import {ErrorDialogComponent} from "./error/error-dialog/error-dialog.component";
import {ApplicationError} from "./error/errors";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  @ViewChild(MatTable, {static: true}) table?: MatTable<Car>;

  title = 'vehicle-price-list';
  displayedColumns: string[] = ['id', 'make', 'model', 'modelEdition', 'price', 'options', 'change'];
  cars: Car[] = [];

  constructor(public dialog: MatDialog, public carService: CarService, public optionsService: CarOptionsService) {
  }

  ngOnInit(): void {
    this.carService.getCars().subscribe(cars => this.cars = cars ? cars : []);
  }

  showCarOptions(car: Car) {

    this.optionsService.getOptions(car.id).subscribe(options => {
      const carForView = _.cloneDeep(car);
      carForView.options = options;

      const dialogRef = this.dialog.open(CarOptionsDialogComponent, {
        width: '450px',
        data: carForView
      });

    });

  }

  deleteCar(carId: number) {
    this.carService.deleteCar(carId).subscribe(() => {
      this.cars = this.cars.filter(car => car.id !== carId);
    }, error => {
       this.dialog.open(ErrorDialogComponent, {
         width: '400px',
         data: new ApplicationError('Could not delete selected car.', error)
       })
    })
  }

  editCar(car: Car) {


    this.optionsService.getOptions(car.id).subscribe(response => {

      const carDialogData = new CarDialogData();
      carDialogData.mode = DialogMode.EDIT;
      carDialogData.data = car;
      carDialogData.optionsData = response;

      const dialogRef = this.dialog.open(CarEditDialogComponent, {
        width: '600px',
        height: '600px',
        data: carDialogData
      });

      dialogRef.afterClosed().subscribe((result: Car) => {

        if (result && result.id) {
          const carObservable = this.carService.updateCar(result);
          const carOptionsObservable = this.optionsService.updateOptions(result.options, result.id);

          carObservable.pipe(mergeMap( result => {

            const idx = this.cars.findIndex(car => car.id === result.id);
            this.cars[idx] = result;

            if (this.table) {
              this.table.renderRows();
            }
            return carOptionsObservable
          })).subscribe(carOptions => {
            console.log('Options were updated', carOptions);
          }, error => {
            this.dialog.open(ErrorDialogComponent, {
              width: '400px',
              data: new ApplicationError('Problem when saving edited car data.', error)
            })
          });
        }

      });

    });
  }

  addNewCar() {

    const carDialogData = new CarDialogData();
    carDialogData.mode = DialogMode.NEW;

    const dialogRef = this.dialog.open(CarEditDialogComponent, {
      width: '600px',
      height: '600px',
      data: carDialogData
    });

    dialogRef.afterClosed().subscribe(dialogResult => {

      if (dialogResult) {
        const carObservable = this.carService.addCar(dialogResult);

        carObservable.pipe(mergeMap( result => {
          this.cars.push(result);
          const carOptionsObservable = this.optionsService.updateOptions(dialogResult.options, result.id);

          if (this.table) {
            this.table.renderRows();
          }
          return carOptionsObservable
        })).subscribe(carOptions => {
          console.log('Options were updated', carOptions);
        }, error => {
          this.dialog.open(ErrorDialogComponent, {
            width: '400px',
            data: new ApplicationError('Problem when saving new car data.', error)
          })
        });
      }

    });
  }
}
