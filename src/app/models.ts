/**
 @author: Jakub Gzyl, 2021
 **/

declare var require: any;

export class DialogMode {

  static NEW = 'NEW';
  static EDIT = 'EDIT';
  static VIEW = 'ADD';

}

export class Car {

  public options: CarOption[] = [];

  constructor(public id: number, public make: string, public model: string, public edition: string, public price?: number) {
  }

}

export class CarOption {

  uiId?: string;
  carId: number = 0;

  constructor(public id?: number, public name?: string, public price?: number) {
  }

  initUiId() {
    const { v4: uuidv4 } = require('uuid');
    this.uiId = uuidv4();
  }
}
