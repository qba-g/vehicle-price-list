/**
 @author: Jakub Gzyl, 2021
 **/

import {Injectable} from '@angular/core';
import {EMPTY, Observable, of} from "rxjs";
import {Car, CarOption} from "./models";
import {environment} from "../environments/environment";
import {take} from "rxjs/operators";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class CarOptionsService {

  static BACKEND_SERVER = environment.backend_server;
  static GET_OPTIONS_URL = '/car/options/{carId}';
  static POST_OPTIONS_URL = '/car/options/{carId}';

  constructor(public httpClient: HttpClient) {
  }

  getOptions(carId: number): Observable<CarOption[]> {

    const url = CarOptionsService.BACKEND_SERVER + CarOptionsService.GET_OPTIONS_URL.replace('{carId}', '' + carId);

    return this.httpClient.get<CarOption[]>(url, {responseType: 'json'}).pipe(take(1));
  }

  updateOptions(list: CarOption[], carId: number): Observable<CarOption[]> {

    if (list) {
      list.forEach(carOption => carOption.carId = carId);
      const url = CarOptionsService.BACKEND_SERVER + CarOptionsService.POST_OPTIONS_URL.replace('{carId}', '' + carId);
      return this.httpClient.post<CarOption[]>(url, list, {responseType: 'json'}).pipe(take(1));
    }
    return EMPTY;
  }
}
